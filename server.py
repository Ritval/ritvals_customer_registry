import socket
import selectors
import struct
from packer import Packer
from server_requests import Admin
from server_requests import ServerRequests


class Server:
    HOST = '127.0.0.1'
    PORT = 5050

    def __init__(self):
        self.requests = ServerRequests()
        self.sel = selectors.DefaultSelector()
        server_socket = socket.socket()
        self.active_connections = []

        server_socket.bind((self.HOST, self.PORT))
        server_socket.listen()
        server_socket.setblocking(0x4000)
        self.sel.register(server_socket, selectors.EVENT_READ, self.accept_connection)

        print('Connected to database.')
        print(f'Server now listening on: {server_socket.getsockname()}')

    def accept_connection(self, sock: socket.socket) -> None:
        conn, addr = sock.accept()
        print(f'Accepted connection from: {addr}')
        conn.setblocking(0x4000)
        self.active_connections.append(conn)
        self.sel.register(conn, selectors.EVENT_READ, self.receive_request)

    def receive_request(self, conn: socket.socket) -> None:
        try:
            packed_message = conn.recv(4096)
            collected_bytes = 4096
            total_bytes, message_bytes = Packer.get_struct_byte_sizes(packed_message)

            while collected_bytes < total_bytes:
                packed_message += conn.recv(4096)
                collected_bytes += 4096

            client_request = Packer.unpack_struct_to_string(packed_message, total_bytes, message_bytes)
            print(f'Received request from client: {conn.getpeername()}')
            self.get_server_reply(conn, client_request)
        except ConnectionResetError:
            self.close_connection(conn)
        except struct.error:
            self.close_connection(conn)

    def get_server_reply(self, conn: socket.socket, client_request: str) -> None:
        deserialized_client_request = Packer.deserialize_from_json(client_request)
        server_reply = self.requests.handle_request(deserialized_client_request)
        self.handle_server_reply(conn, deserialized_client_request, server_reply)

    def handle_server_reply(self, conn: socket.socket, deserialized_client_request, server_reply: str) -> None:

        if Admin.broadcast_message:
            self.broadcast(conn, Admin.broadcast_message)
            Admin.broadcast_message = ''

        if deserialized_client_request.get('format') != 'db':
            server_reply = Packer.serialize_to_json(server_reply)

        print(
            f'Responding to client: {conn.getpeername()}, command: "{deserialized_client_request["command"]}"')
        packed_response = Packer.pack_struct_message(server_reply)
        conn.sendall(packed_response)

    def broadcast(self, broadcasters_conn: socket.socket, response) -> None:
        response = Packer.serialize_to_json(response)
        packed_response = Packer.pack_struct_message(response)
        for connection in self.active_connections:
            if connection != broadcasters_conn:
                connection.sendall(packed_response)

    def close_connection(self, conn: socket.socket) -> None:
        print(f'Closing connection with: {conn.getpeername()}')
        self.active_connections.remove(conn)
        self.sel.unregister(conn)
        conn.close()

    def server_loop(self) -> None:
        while True:
            events = self.sel.select()
            for key, _ in events:
                callback = key.data
                callback(key.fileobj)


if __name__ == '__main__':
    server = Server()
    server.server_loop()
