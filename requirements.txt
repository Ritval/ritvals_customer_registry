Faker==9.7.1
autopep8==1.5.7
coverage==6.1.1
flake8==3.9.2
tox==3.24.4
pytest==6.2.5