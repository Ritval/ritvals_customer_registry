from server_requests import Admin


def test_admin_default_not_locked():
    admin = Admin()
    assert not admin.database_lock


def test_lock_incorrect_password():
    admin = Admin()
    admin.lock('admin5')
    assert not admin.database_lock


def test_lock_incorrect_password_broadcast_message():
    admin = Admin()
    admin.lock('admin12')
    assert not admin.broadcast_message


def test_execute_request_lock_incorrect_password():
    admin = Admin()
    request = {'command': 'lock', 'password': 'aasdasd'}
    result = admin.execute_request(request)
    assert result == 'Failed to lock database. Invalid password.'


def test_execute_request_unlock_incorrect_password():
    admin = Admin()
    request = {'command': 'unlock', 'password': 'admdsmin'}
    result = admin.execute_request(request)
    assert result == 'Failed to unlock database. Invalid password.'


def test_lock_correct_password():
    admin = Admin()
    admin.lock('admin')
    assert admin.database_lock


def test_unlock_correct_password():
    admin = Admin()
    admin.unlock('admin')
    assert not admin.database_lock


def test_lock_correct_password_broadcast_message():
    admin = Admin()
    admin.lock('admin')
    assert admin.broadcast_message == 'The database is now locked.'


def test_execute_request_lock_correct_password():
    admin = Admin()
    request = {'command': 'lock', 'password': 'admin'}
    result = admin.execute_request(request)
    assert result == 'Access granted! The database is now locked.'


def test_execute_request_unlock_correct_password():
    admin = Admin()
    request = {'command': 'unlock', 'password': 'admin'}
    result = admin.execute_request(request)
    assert result == 'Access granted! The database is now unlocked.'
