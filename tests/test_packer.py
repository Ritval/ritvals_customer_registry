from packer import Packer


def test_pack_struct_message():
    packed_message = Packer.pack_struct_message('test')
    assert packed_message == b'\x00\x00\x00\x08test'


def test_pack_struct_message_bytes():
    packed_message = Packer.pack_struct_message(b'test')
    assert packed_message == b'\x00\x00\x00\x08test'


def test_get_struct_byte_size_total():
    packed_message = Packer.pack_struct_message('test123')
    total_bytes, _ = Packer.get_struct_byte_sizes(packed_message)
    assert total_bytes == 11


def test_get_struct_byte_size():
    packed_message = Packer.pack_struct_message('test123')
    _, message_bytes = Packer.get_struct_byte_sizes(packed_message)
    assert message_bytes == 7


def test_unpack_struct_message_to_string():
    packed_message = Packer.pack_struct_message('teest')
    total_bytes, message_bytes = Packer.get_struct_byte_sizes(packed_message)
    message = Packer.unpack_struct_to_string(packed_message, total_bytes, message_bytes)
    assert message == 'teest'


def test_unpack_struct_message_to_bytes():
    packed_message = Packer.pack_struct_message('teest')
    total_bytes, message_bytes = Packer.get_struct_byte_sizes(packed_message)
    message = Packer.unpack_struct_to_bytes(packed_message, total_bytes, message_bytes)
    assert message == b'teest'


def test_serialize_to_json():
    message = {'test123': 'test'}
    serialized_message = Packer.serialize_to_json(message)
    assert isinstance(serialized_message, str)


def test_deserialize_to_json():
    message = {'test123': 'test'}
    serialized_message = Packer.serialize_to_json(message)
    deserialized_message = Packer.deserialize_from_json(serialized_message)
    assert isinstance(deserialized_message, dict)


def test_format_query_to_json():
    customers_list = [['1', 'tester', 'testington', 'testcorp', '070701']]
    customers_dict = Packer.format_query_to_json(customers_list)
    assert customers_dict == {'customer1': {'first_name': 'tester', 'last_name': 'testington',
                                            'company': 'testcorp', 'phone_number': '070701'}}


def test_format_query_to_json_multiple():
    customers_list = [['1', 'tester', 'testington', 'testcorp', '070701'],
                      ['2', 'tester2', 'testington2', 'testcorp2', '0707012']]
    customers_dict = Packer.format_query_to_json(customers_list)
    assert customers_dict['customer2'] == {'first_name': 'tester2', 'last_name': 'testington2',
                                           'company': 'testcorp2', 'phone_number': '0707012'}
