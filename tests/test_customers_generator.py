from customer_generator import CustomerGenerator


def test_customer_amount_generated():
    customers = CustomerGenerator.generate_customer_info(5)
    assert len(customers) == 5


def test_first_name_is_string():
    customers = CustomerGenerator.generate_customer_info(1)
    assert isinstance(customers['customer1']['first_name'], str)
