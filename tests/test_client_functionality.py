from client_functionality import Display, SearchMenu, CustomersMenu


def test_display_query_result(capfd):
    customer = [['2', 'test', 'testington', 'testcorp', '010203']]
    Display.display_query_result(customer)
    out, err = capfd.readouterr()
    assert out == ('Customer ID: 2\nName: test testington\nCompany: testcorp\n'
                   'Phone number: 010203\n\n')


def test_get_exact_search_dict():
    search_menu = SearchMenu()
    result = search_menu.get_exact_search_dict('test123', 'testington')
    assert result == {'searched_column': 'test123',
                      'searched_value': 'testington', 'request_type': 'db_read',
                      'command': 'query_search', 'request_type': 'db_read'}


def test_get_starts_with_search_dict():
    search_menu = SearchMenu()
    result = search_menu.get_starts_with_search_dict('test123', 'testington')
    assert result == {'searched_column': 'test123',
                      'searched_value': 'testington%', 'request_type': 'db_read',
                      'command': 'query_search', 'request_type': 'db_read'}


def test_get_delete_customer_dict():
    customers_menu = CustomersMenu()
    result = customers_menu.get_delete_customer_dict(5)
    assert result == {'command': 'delete', 'delete_id': 5, 'request_type': 'db_write'}


def test_get_new_customer_dict():
    customers_menu = CustomersMenu()
    result = customers_menu.get_new_customer_dict('Test', 'Testington', 'Test company', '010203')
    assert result == {'first_name': 'Test', 'last_name': 'Testington', 'company': 'Test company',
                      'phone_number': '010203', 'command': 'insert', 'request_type': 'db_write'}
