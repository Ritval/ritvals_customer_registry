from file_handler import FileHandler


def test_write_read(tmpdir):
    FileHandler.write_file_json(tmpdir, 'temp.json', {'test': 'test123123'})
    assert FileHandler.get_json_content(tmpdir, 'temp.json') == {'test': 'test123123'}


def test_write_read_bytes(tmpdir):
    FileHandler.write_file_bytes(tmpdir, 'temp.bin', b'haha')
    data = FileHandler.read_file_as_bytes(tmpdir, 'temp.bin')
    assert data == b'haha'


def test_delete_file(tmpdir):
    FileHandler.write_file_json(tmpdir, 'temp.json', {'test123': 'test'})
    FileHandler.delete_file(tmpdir, 'temp.json')
    assert len(tmpdir.listdir()) == 0


def test_get_if_file_exists(tmpdir):
    FileHandler.write_file_json(tmpdir, 'temp.json', {'hehe': 'test'})
    assert FileHandler.get_if_file_exists(tmpdir, 'temp.json')


def test_create_folder(tmpdir):
    dirname = tmpdir.join('testdir')
    FileHandler.create_folder(dirname)
    assert len(tmpdir.listdir()) == 1
