import faker


class CustomerGenerator:

    @staticmethod
    def generate_customer_info(amount: int) -> dict:
        fake = faker.Faker()
        new_customers = {}
        for i in range(1, amount + 1):
            first_name = fake.first_name()
            last_name = fake.last_name()
            company = fake.company()
            phone_number = fake.phone_number()
            new_customers[f'customer{i}'] = {
                'first_name': first_name,
                'last_name': last_name,
                'company': company,
                'phone_number': phone_number
            }
        return new_customers
