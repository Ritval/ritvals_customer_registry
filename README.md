# RITVAL's final project: Customer registry

### Description of project:
* Server and client which communicate over sockets in Python. 
* The clients are used to interact with a SQLite database through the server by using a CLI menu.
* Handles sending large amounts of data using structs.
* Generate up to 20.000 fake customers at a time and upload them to the database!
* Lock or unlock the database to prevent any unwanted changes, the status is broadcasted to all active clients.

### How to run:
* pip install -r requirements.txt
* Run the '<a href=https://gitlab.com/Ritval/devops21_python_slutuppgift/-/blob/main/server.py>server.py</a>' and '<a href=https://gitlab.com/Ritval/devops21_python_slutuppgift/-/blob/main/client.py>client.py</a>' in separate consoles of your choice.
* You can run multiple clients at the same time by running them from separate consoles.
* The password to lock/unlock the database is 'admin'

### File summaries:
* '<a href=https://gitlab.com/Ritval/devops21_python_slutuppgift/-/blob/main/server.py>server.py</a>'
  
A server constantly waiting for client connections using selectors. 
Listens for new requests from clients and handles them with the help of the ServerRequests class and subsequently sends them a reply.

* '<a href="https://gitlab.com/Ritval/devops21_python_slutuppgift/-/blob/main/server_requests.py">server_requests.py</a>'

The <b>ServerRequests</b> class receives a dictionary containing keys and values depending on the clients request.
Checks the request type stored in the dictionary, creates an instance of the corresponding child class and forwards the request dictionary.

The <b>Admin</b> class manages the server lock and message broadcasting for admin functionality. Checks the password and command in the clients request dictionary and responds accordingly.
   
* '<a href=https://gitlab.com/Ritval/devops21_python_slutuppgift/-/blob/main/client.py>client.py</a>'

A client which connects to the server and lets the user send and receive requests using threading and a CLI menu created with the MainMenu class.

* '<a href="https://gitlab.com/Ritval/devops21_python_slutuppgift/-/blob/main/client_functionality.py">client_functionality.py</a>'

<b>MainMenu</b> is the main menu class for the client. It gives the user a CLI menu and subsequently creates instances and starts sub menus depending on the clients selection.
   
The <b>HelperFunctions</b> class provides simple input functions for the menus.
   
The <b>Display</b> class provides functionality for displaying query results in a more structured format to the client.


* '<a href="https://gitlab.com/Ritval/devops21_python_slutuppgift/-/blob/main/packer.py">packer.py</a>'

Functions for packing and serializing data using struct and json.


* '<a href="https://gitlab.com/Ritval/devops21_python_slutuppgift/-/blob/main/file_handler.py">file_handler.py</a>'


Functions for reading, writing and handling files. Also stores the paths to the different directories.

* '<a href="https://gitlab.com/Ritval/devops21_python_slutuppgift/-/blob/main/database.py">database.py</a>'

Contains the <b>CustomersTableFunctions</b> class which is used to interact with the customers table in our database.

* '<a href="https://gitlab.com/Ritval/devops21_python_slutuppgift/-/blob/main/database_conn.py">database_conn.py</a>'

A singleton implementation of a connection instance to our database. 'db_conn' is imported to the database and is used for executing queries.

### Dependencies:
* Python 3.8+ (using the Walrus operator)
* Requirements.txt:
```
Faker==9.7.1
autopep8==1.5.7
coverage==6.1.1
flake8==3.9.2
tox==3.24.4
pytest==6.2.5
```
### settings.json used for this project (.vscode):
```py
{
    "python.testing.pytestArgs": [
        "-v",
        "-s",
        ".",
        "-p",
        "test*.py"
    ],
    "python.testing.unittestEnabled": false,
    "python.testing.pytestEnabled": true,
    "editor.formatOnSave": true,
    "python.linting.enabled": true,
    "python.linting.flake8Enabled": true
}
```