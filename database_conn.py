from file_handler import FileHandler
import sqlite3


class DatabaseConnector(sqlite3.Connection):
    # Singleton
    def __init__(self):
        FileHandler.create_folder(FileHandler.SERVER_DATA_DIR)
        super().__init__(FileHandler.DATABASE_PATH)


db_conn = DatabaseConnector()
