import struct
import json


class Packer:

    @staticmethod
    def pack_struct_message(message) -> bytes:
        if not isinstance(message, bytes):
            message = message.encode()
        struct_format = f'!L{len(message)}s'
        struct_size = struct.calcsize(struct_format)
        packed_message = struct.pack(struct_format, struct_size, message)
        return packed_message

    @staticmethod
    def unpack_struct_to_string(packed_message: bytes, total_bytes: int, message_bytes: int) -> str:
        message_in_bytes, = struct.unpack(f'{message_bytes}s', packed_message[4:total_bytes])
        message = message_in_bytes.decode()
        return message

    @staticmethod
    def unpack_struct_to_bytes(packed_message: bytes, total_bytes: int, message_bytes: int) -> bytes:
        message_in_bytes, = struct.unpack(f'{message_bytes}s', packed_message[4:total_bytes])
        return message_in_bytes

    @staticmethod
    def get_struct_byte_sizes(packed_message: bytes) -> tuple:
        total_bytes, = struct.unpack('!L', packed_message[:4])
        message_bytes = total_bytes - 4
        return total_bytes, message_bytes

    @staticmethod
    def serialize_to_json(message) -> str:
        serialized_message = json.dumps(message)
        return serialized_message

    @staticmethod
    def deserialize_from_json(message: str):
        deserialized_message = json.loads(message)
        return deserialized_message

    @staticmethod
    def format_query_to_json(customers: list) -> dict:
        result = {}
        for customer in customers:
            result[f'customer{customer[0]}'] = {
                'first_name': customer[1],
                'last_name': customer[2],
                'company': customer[3],
                'phone_number': customer[4]
            }
        return result
