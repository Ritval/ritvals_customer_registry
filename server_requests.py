from database import CustomersTableFunctions
from file_handler import FileHandler
from packer import Packer


class ServerRequests:

    def __init__(self):
        self.customers = CustomersTableFunctions()

    def handle_request(self, request: dict):
        request_type = request['request_type']

        if request_type == 'db_read':
            reader = ReadDb()
            result = reader.execute_request(request)
        elif request_type == 'db_write':
            editor = WriteDb()
            result = editor.execute_request(request)
        elif request_type == 'admin':
            admin = Admin()
            result = admin.execute_request(request)

        return result


class ReadDb(ServerRequests):

    def execute_request(self, request: dict):
        command = request['command']

        if command == 'query_all':
            result = self.query_all()
        elif command == 'download':
            result = self.download(request)
        elif command == 'query_search':
            result = self.query_search(request)
        return result

    def query_all(self):
        result = self.customers.query_all()
        if not result:
            return 'You currently have no customers.'
        return result

    def download(self, request: dict):
        if request['format'] == 'json':
            result = self.customers.query_all()
            json_result = Packer.format_query_to_json(result)
            formatted_result = {'download_json': json_result}
            return formatted_result
        elif request['format'] == 'db':
            return FileHandler.read_file_as_bytes(FileHandler.SERVER_DATA_DIR, 'database.db')

    def query_search(self, request: dict):
        searched_column = request['searched_column']
        searched_value = request['searched_value']
        result = self.customers.query_specific(searched_column, searched_value)
        if not result:
            return 'Your search generated no hits.'
        return result


class WriteDb(ServerRequests):

    def execute_request(self, request: dict) -> str:
        if Admin.database_lock:
            return 'Database is currently locked, no changes are allowed.'

        command = request['command']

        if command == 'insert':
            result = self.insert(request)
        elif command == 'upload':
            result = self.upload(request)
        elif command == 'delete':
            result = self.delete(request)
        elif command == 'drop':
            result = self.drop()

        return result

    def insert(self, request: dict) -> str:
        first_name = request['first_name'].title()
        last_name = request['last_name'].title()
        company = request['company'].title()
        phone_number = request['phone_number']
        self.customers.insert_customer(first_name, last_name, company, phone_number)
        return f'Successfully added customer {first_name} {last_name} to the database!'

    def upload(self, request: dict) -> str:
        file_content = request['file_content']
        self.customers.handle_json_upload(file_content)
        return 'Successfully uploaded your customers to the database!'

    def delete(self, request: dict) -> str:
        delete_id = request['delete_id']
        if self.customers.query_if_customer_exists(delete_id):
            self.customers.delete_customer(delete_id)
            return f"Deleted customer with ID {delete_id}!"
        return f'No customer with ID {delete_id} to delete.'

    def drop(self) -> str:
        self.customers.drop_table()
        self.customers.create_table()
        return "The customers table is now empty!"


class Admin:

    database_lock = False
    broadcast_message = ''

    def execute_request(self, request: dict) -> None:
        command = request['command']
        client_password = request['password']

        if command == 'lock':
            result = self.lock(client_password)
        elif command == 'unlock':
            result = self.unlock(client_password)

        return result

    def lock(self, client_password: str) -> str:
        if client_password == 'admin':
            Admin.database_lock = True
            Admin.broadcast_message = 'The database is now locked.'
            return 'Access granted! The database is now locked.'
        return 'Failed to lock database. Invalid password.'

    def unlock(self, client_password: str) -> str:
        if client_password == 'admin':
            Admin.database_lock = False
            Admin.broadcast_message = 'The database is now unlocked.'
            return 'Access granted! The database is now unlocked.'
        return 'Failed to unlock database. Invalid password.'
