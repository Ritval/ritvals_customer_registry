from customer_generator import CustomerGenerator
from file_handler import FileHandler


class MainMenu:

    def prompt_menu(self) -> dict:
        CUSTOMERS_MENU = '1'
        FILES_MENU = '2'
        LOCK_DB = '3'
        UNLOCK_DB = '4'

        while (client_input := input(self.get_main_menu_string()).lower()) != 'q':

            if client_input == CUSTOMERS_MENU:
                client_request = self.start_customers_menu()
                if client_request is not None:
                    return client_request

            elif client_input == FILES_MENU:
                client_request = self.start_files_menu()
                if client_request is not None:
                    return client_request

            elif client_input == LOCK_DB:
                client_request = self.lock_db()
                return client_request

            elif client_input == UNLOCK_DB:
                client_request = self.unlock_db()
                return client_request
            else:
                print('Invalid command.')

    def start_customers_menu(self):
        customers_menu = CustomersMenu()
        client_request = customers_menu.prompt_menu()
        if client_request is not None:
            return client_request

    def start_files_menu(self):
        files_menu = FilesMenu()
        client_request = files_menu.prompt_menu()
        if client_request is not None:
            return client_request

    def lock_db(self):
        password = HelperFunctions.prompt_not_empty_input('Enter password: ')
        client_request = {'command': 'lock', 'request_type': 'admin', 'password': password}
        return client_request

    def unlock_db(self):
        password = HelperFunctions.prompt_not_empty_input('Enter password: ')
        client_request = {'command': 'unlock', 'request_type': 'admin', 'password': password}
        return client_request

    def get_main_menu_string(self) -> str:
        return (
            '\nMAIN MENU\n[Q]: Exit.\n[1]: Customers menu.\n[2]: Files menu.\n[3]: Lock database.'
            '\n[4]: Unlock database.\n'
        )


class CustomersMenu:

    def prompt_menu(self) -> dict:
        QUERY_ALL = '1'
        QUERY_SEARCH = '2'
        INSERT_NEW = '3'
        DELETE = '4'
        DROP = '5'

        while (client_input := input(self.get_customers_menu_string()).lower()) != 'b':

            if client_input == QUERY_ALL:
                client_request = {'command': 'query_all', 'request_type': 'db_read'}
                return client_request

            elif client_input == QUERY_SEARCH:
                client_request = self.query_search()
                if client_request is not None:
                    return client_request

            elif client_input == INSERT_NEW:
                client_request = self.insert_new()
                return client_request

            elif client_input == DELETE:
                client_request = self.delete()
                return client_request

            elif client_input == DROP:
                client_request = self.drop()
                if client_request is not None:
                    return client_request

            else:
                print("Invalid command.")

    def query_search(self):
        search_menu = SearchMenu()
        client_request = search_menu.prompt_menu()
        if client_request is not None:
            return client_request

    def insert_new(self):
        client_request = self.prompt_new_customer()
        return client_request

    def delete(self):
        id = HelperFunctions.prompt_input_int('Enter customer ID to delete: ')
        client_request = self.get_delete_customer_dict(id)
        return client_request

    def drop(self):
        decision = input('Are you sure? (Y): ').lower()
        if decision == 'y':
            client_request = {'command': 'drop', 'request_type': 'db_write'}
            return client_request

    def prompt_new_customer(self) -> dict:
        client_request = {}
        print("\nREGISTERING NEW CUSTOMER")
        first_name = HelperFunctions.prompt_not_empty_input('First name: ')
        last_name = HelperFunctions.prompt_not_empty_input('Last name: ')
        company = HelperFunctions.prompt_not_empty_input('Company name: ')
        phone_number = HelperFunctions.prompt_not_empty_input('Phone number: ')
        client_request = self.get_new_customer_dict(first_name, last_name, company, phone_number)
        return client_request

    def get_delete_customer_dict(self, id):
        delete_customer = {'command': 'delete', 'delete_id': id, 'request_type': 'db_write'}
        return delete_customer

    def get_new_customer_dict(self, first_name: str, last_name: str, company: str, phone_number: str) -> dict:
        new_customer = {'first_name': first_name, 'last_name': last_name, 'company': company,
                        'phone_number': phone_number, 'command': 'insert', 'request_type': 'db_write'}
        return new_customer

    def get_customers_menu_string(self) -> str:
        return (
            '\nCUSTOMERS MENU\n[B]: Back to main menu.\n[1]: Display all customers.\n[2]: Query a specific customer.'
            '\n[3]: Register new customer.\n[4]: Delete a customer by ID.\n[5]: Empty the customer database.\n'
        )


class SearchMenu:

    def prompt_menu(self) -> dict:
        ID = '1'
        FIRST_NAME = '2'
        LAST_NAME = '3'
        COMPANY = '4'
        PHONE = '5'

        while (client_input := input(self.get_search_menu_string()).lower()) != 'b':

            if client_input == ID:
                client_request = self.search_id()
                return client_request

            elif client_input == FIRST_NAME:
                client_request = self.search_first_name()
                return client_request

            elif client_input == LAST_NAME:
                client_request = self.search_last_name()
                return client_request

            elif client_input == COMPANY:
                client_request = self.search_company()
                return client_request

            elif client_input == PHONE:
                client_request = self.search_phone()
                return client_request

            else:
                print('Invalid command.')

    def search_id(self):
        searched_value = HelperFunctions.prompt_input_int('Enter searched ID: ')
        client_request = self.get_exact_search_dict('id', searched_value)
        return client_request

    def search_first_name(self):
        searched_value = HelperFunctions.prompt_not_empty_input('Enter searched value: ')
        client_request = self.get_starts_with_search_dict('first_name', searched_value)
        return client_request

    def search_last_name(self):
        searched_value = HelperFunctions.prompt_not_empty_input('Enter searched value: ')
        client_request = self.get_starts_with_search_dict('last_name', searched_value)
        return client_request

    def search_company(self):
        searched_value = HelperFunctions.prompt_not_empty_input('Enter searched value: ')
        client_request = self.get_starts_with_search_dict('company', searched_value)
        return client_request

    def search_phone(self):
        searched_value = HelperFunctions.prompt_not_empty_input('Enter searched value: ')
        client_request = self.get_starts_with_search_dict('phone_number', searched_value)
        return client_request

    def get_exact_search_dict(self, searched_column: str, searched_value: str):
        search_request = {'searched_column': f'{searched_column}', 'searched_value': searched_value,
                          'request_type': 'db_read', 'command': 'query_search', 'request_type': 'db_read'}
        return search_request

    def get_starts_with_search_dict(self, searched_column: str, searched_value: str):
        search_request = {'searched_column': f'{searched_column}',
                          'searched_value': f'{searched_value}%', 'request_type': 'db_read',
                          'command': 'query_search', 'request_type': 'db_read'}
        return search_request

    def get_search_menu_string(self) -> str:
        return (
            '\nSEARCH CUSTOMERS\n[B]: Back to customers menu.\n[1]: ID.'
            '\n[2]: First name.\n[3]: Last name.\n[4]: Company.\n[5]: Phone number\n'
        )


class FilesMenu:

    def prompt_menu(self) -> dict:
        DOWNLOAD = '1'
        UPLOAD = '2'
        GENERATE = '3'

        while (client_input := input(self.get_files_menu_string()).lower()) != 'b':
            if client_input == DOWNLOAD:
                client_request = self.download()
                if client_request is not None:
                    return client_request

            elif client_input == UPLOAD:
                client_request = self.upload()
                if client_request is not None:
                    return client_request

            elif client_input == GENERATE:
                self.generate()

            else:
                print('Invalid command.')

    def download(self):
        download_menu = DownloadMenu()
        client_request = download_menu.prompt_menu()
        if client_request is not None:
            return client_request

    def upload(self):
        try:
            file_content = FileHandler.get_json_content(FileHandler.CLIENT_DATA_DIR, 'customers.json')
            client_request = {'command': 'upload', 'file_content': file_content, 'request_type': 'db_write'}
            return client_request
        except FileNotFoundError:
            print('Please generate a "customers.json" file before uploading.')

    def generate(self):
        customers_amount = HelperFunctions.prompt_input_int('Enter the amount of customers to generate: ')
        if customers_amount > 20000:
            print('\nYou\'re not allowed to generate more than 20000 customers at a time.')
        else:
            try:
                if FileHandler.get_if_file_exists(FileHandler.CLIENT_DATA_DIR, 'customers.json'):
                    FileHandler.delete_file(FileHandler.CLIENT_DATA_DIR, 'customers.json')

                new_customers = CustomerGenerator.generate_customer_info(customers_amount)
                FileHandler.write_file_json(FileHandler.CLIENT_DATA_DIR, 'customers.json', new_customers)
                print('\nSuccessfully generated a new "customers.json"!')

            except FileNotFoundError:
                print('Created a new "client_data" directory and generated a new "customers.json"!')
                FileHandler.create_folder(FileHandler.CLIENT_DATA_DIR)
                FileHandler.write_file_json(FileHandler.CLIENT_DATA_DIR, 'customers.json', new_customers)

    def get_files_menu_string(self) -> str:
        return (
            '\nFILES MENU\n[B]: Back to main menu.\n[1]: Download customer data stored on the server.'
            '\n[2]: Upload customers from your "customers.json" file.'
            '\n[3]: Generate a new "customers.json" file.\n'
        )


class DownloadMenu:

    def prompt_menu(self) -> dict:
        JSON = '1'
        DB = '2'

        while (client_input := input(self.get_download_menu_string()).lower()) != 'b':

            if client_input == JSON:
                client_request = {'format': 'json', 'command': 'download', 'request_type': 'db_read'}
                return client_request
            elif client_input == DB:
                client_request = {'format': 'db', 'command': 'download', 'request_type': 'db_read'}
                return client_request
            else:
                print('Invalid command.')

    def get_download_menu_string(self):
        return (
            '\nDOWNLOAD MENU\n[B]: Back to files menu.\n[1]: Human readable JSON format (.json).'
            '\n[2]: Database format (.db).\n'
        )


class HelperFunctions:

    @staticmethod
    def prompt_not_empty_input(optional_message='') -> str:
        while True:
            client_input = input(optional_message)
            if not client_input:
                continue
            return client_input

    @staticmethod
    def prompt_input_int(optional_message='') -> int:
        while True:
            client_input = input(optional_message)
            if client_input.isdigit():
                return int(client_input)
            else:
                print('\nInvalid number, try again.')


class Display:

    @staticmethod
    def display_query_result(query_result: list):
        for customer in query_result:
            id = customer[0]
            first_name = customer[1]
            last_name = customer[2]
            company = customer[3]
            phone_number = customer[4]
            print(f'Customer ID: {id}\nName: {first_name} {last_name}\nCompany: {company}\n'
                  f'Phone number: {phone_number}\n')
