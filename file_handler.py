import os
import json


class FileHandler:
    DIRNAME = os.path.dirname(__file__)
    CLIENT_DATA_DIR = os.path.join(DIRNAME, 'client_data')
    SERVER_DATA_DIR = os.path.join(DIRNAME, 'server_data')
    DATABASE_PATH = os.path.join(SERVER_DATA_DIR, 'database.db')

    @staticmethod
    def get_json_content(dirname: str, filename: str):
        file_path = os.path.join(dirname, filename)
        with open(file_path) as f:
            data = json.load(f)
        return data

    @staticmethod
    def read_file_as_bytes(dirname: str, filename: str) -> bytes:
        file_path = os.path.join(dirname, filename)
        data_list = []
        with open(file_path, 'rb') as f:
            while True:
                data = f.read(4096)
                if not data:
                    break
                data_list.append(data)
        data = b''.join(data_list)
        return data

    @staticmethod
    def write_file_json(dirname: str, filename: str, data: dict) -> None:
        file_path = os.path.join(dirname, filename)
        with open(file_path, 'w') as f:
            json.dump(data, f, indent=4)

    @staticmethod
    def write_file_bytes(dirname: str, filename: str, data: bytes) -> None:
        file_path = os.path.join(dirname, filename)
        with open(file_path, 'wb') as f:
            f.write(data)

    @staticmethod
    def delete_file(dirname: str, filename: str) -> None:
        file_path = os.path.join(dirname, filename)
        os.remove(file_path)

    @staticmethod
    def get_if_file_exists(dirname: str, filename: str) -> bool:
        file_path = os.path.join(dirname, filename)
        return os.path.isfile(file_path)

    @staticmethod
    def create_folder(DIR_PATH) -> None:
        os.makedirs(DIR_PATH, exist_ok=True)
