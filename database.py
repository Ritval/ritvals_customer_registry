from database_conn import db_conn


class CustomersTableFunctions:
    _CREATE_CUSTOMERS_TABLE = (
        'CREATE TABLE IF NOT EXISTS customers '
        '(id INTEGER PRIMARY KEY, first_name TEXT, last_name TEXT, company TEXT, phone_number TEXT);'
    )
    _INSERT_CUSTOMER = 'INSERT INTO customers (first_name, last_name, company, phone_number) VALUES (?, ?, ?, ?);'
    _DROP_CUSTOMERS = 'DROP TABLE customers;'
    _DELETE_CUSTOMER_BY_ID = 'DELETE FROM customers WHERE id = ?;'
    _QUERY_ALL_CUSTOMERS = 'SELECT * FROM customers;'
    _QUERY_CUSTOMERS_AMOUNT = 'SELECT count(id) from customers;'
    _QUERY_IF_CUSTOMER_ID_EXISTS = "SELECT id FROM customers WHERE id = ?"

    def __init__(self):
        self.create_table()

    def create_table(self) -> None:
        with db_conn:
            db_conn.execute(self._CREATE_CUSTOMERS_TABLE)

    def insert_customer(self, first_name: str, last_name: str, company: str, phone_number: str) -> None:
        with db_conn:
            db_conn.execute(self._INSERT_CUSTOMER, (first_name, last_name, company, phone_number))

    def drop_table(self) -> None:
        with db_conn:
            db_conn.execute(self._DROP_CUSTOMERS)

    def delete_customer(self, delete_id: str) -> None:
        with db_conn:
            db_conn.execute(self._DELETE_CUSTOMER_BY_ID, (delete_id,))

    def query_all(self) -> list:
        with db_conn:
            result = db_conn.execute(self._QUERY_ALL_CUSTOMERS).fetchall()
            return result

    def query_amount(self) -> list:
        with db_conn:
            result = db_conn.execute(self._QUERY_CUSTOMERS_AMOUNT)
        return result

    def query_if_customer_exists(self, customer_id: int) -> bool:
        with db_conn:
            result = db_conn.execute(self._QUERY_IF_CUSTOMER_ID_EXISTS, (customer_id,)).fetchall()
        if len(result) == 0:
            return False
        return True

    def query_specific(self, searched_column: str, searched_value: str) -> list:
        query_search = (f'SELECT * FROM customers WHERE {searched_column} LIKE (?);')
        with db_conn:
            result = db_conn.execute(query_search, (searched_value,)).fetchall()
        return result

    def handle_json_upload(self, file_content):
        with db_conn:
            for customer in file_content:
                first_name = file_content[customer]['first_name']
                last_name = file_content[customer]['last_name']
                company = file_content[customer]['company']
                phone_number = file_content[customer]['phone_number']
                db_conn.execute(self._INSERT_CUSTOMER, (first_name, last_name, company, phone_number))
