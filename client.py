import socket
import threading
import struct
import os
from client_functionality import MainMenu, Display
from file_handler import FileHandler
from packer import Packer


class Client:
    HOST = '127.0.0.1'
    PORT = 5050

    def __init__(self):
        FileHandler.create_folder(FileHandler.CLIENT_DATA_DIR)
        self.sock = socket.socket()
        self.menu = MainMenu()
        self.event_thread = threading.Event()
        receive_thread = threading.Thread(target=self.receive_message, daemon=True)
        self.sock.connect((self.HOST, self.PORT))
        receive_thread.start()

    def receive_message(self) -> None:
        while True:
            try:
                packed_message = self.sock.recv(4096)
                collected_bytes = 4096
                total_bytes, message_bytes = Packer.get_struct_byte_sizes(packed_message)

                while collected_bytes < total_bytes:
                    packed_message += self.sock.recv(4096)
                    collected_bytes += 4096

                self.handle_received_message(packed_message, total_bytes, message_bytes)
            except ConnectionResetError:
                print('The server has been shut down. Exiting...')
                os._exit(0)
            except struct.error:
                print('Something went wrong when retrieving data from the server. Exiting...')
                os._exit(0)

    def handle_received_message(self, packed_message: bytes, total_bytes: int, message_bytes: int) -> None:
        try:
            response = Packer.unpack_struct_to_string(packed_message, total_bytes, message_bytes)
            response = Packer.deserialize_from_json(response)

            if isinstance(response, dict):
                FileHandler.write_file_json(FileHandler.CLIENT_DATA_DIR, 'customers.json',
                                            response.get('download_json'))
                print('\nSuccessfully downloaded .json file from server!')

            elif isinstance(response, list):
                Display.display_query_result(response)

            else:
                print(response)

        except UnicodeDecodeError:
            db_info = Packer.unpack_struct_to_bytes(packed_message, total_bytes, message_bytes)
            FileHandler.write_file_bytes(FileHandler.CLIENT_DATA_DIR, 'database.db', db_info)
            print('\nSuccessfully downloaded .db file from server!')

        self.event_thread.set()
        self.event_thread.clear()

    def client_loop(self) -> None:
        while True:
            client_request = self.menu.prompt_menu()
            if client_request is None:
                break
            client_request = Packer.serialize_to_json(client_request)
            packed_message = Packer.pack_struct_message(client_request)
            self.sock.sendall(packed_message)
            self.event_thread.wait()


if __name__ == '__main__':
    client = Client()
    client.client_loop()
